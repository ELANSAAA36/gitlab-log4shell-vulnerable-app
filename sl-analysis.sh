#!/bin/sh

#### Analyze code
BASEDIR=$(dirname $0)
echo "Script location: ${CI_PROJECT_DIR}"

# run analysis (Qwiet SAST) - post build 
sl analyze \
  --verbose \
  --app "$CI_PROJECT_NAME" \
  --oss-project-dir "$CI_PROJECT_DIR" \
  --tag branch="$CI_COMMIT_REF_NAME" \
  --wait \
  .

# echo "Got merge request '$CI_MERGE_REQUEST_IID' for branch $CI_COMMIT_REF_NAME"
# sl check-analysis \
#   --config ./shiftleft.yml \
#   --app "$CI_PROJECT_NAME" \
#   --report-file /tmp/check-analysis.md \
#   --source "tag.branch=main" \
#   --target "tag.branch=$CI_COMMIT_REF_NAME"

# # workflow step example : saving check analysis output as a MR comment 
# CHECK_ANALYSIS_OUTPUT=$(cat /tmp/check-analysis.md)
# COMMENT_BODY=$(jq -n --arg body "$CHECK_ANALYSIS_OUTPUT" '{body: $body}')

# # Post report as merge request comment
# curl -i -XPOST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
#   -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
#   -H "Content-Type: application/x-www-form-urlencoded" \
#   -d "body=$CHECK_ANALYSIS_OUTPUT"

# Check if this is running in a merge request
if [ -n "$CI_MERGE_REQUEST_IID" ]; then
  echo "Got merge request $CI_MERGE_REQUEST_IID for branch $CI_COMMIT_REF_NAME"

  # Run check-analysis and save report to /tmp/check-analysis.md (references shiftleft.yml at root of repo for build rules)
  sl check-analysis \
    --config ./shiftleft.yml \
    --app "$CI_PROJECT_NAME" \
    --report-file /tmp/check-analysis.md \
    --source "tag.branch=main" \
    --target "tag.branch=$CI_COMMIT_REF_NAME"

  # workflow step example : saving check analysis output as a MR comment 
  CHECK_ANALYSIS_OUTPUT=$(cat /tmp/check-analysis.md)
  COMMENT_BODY=$(jq -n --arg body "$CHECK_ANALYSIS_OUTPUT" '{body: $body}')
  # Post report as merge request comment
  curl -i -XPOST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
    -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "body=$CHECK_ANALYSIS_OUTPUT"

fi


exit 0